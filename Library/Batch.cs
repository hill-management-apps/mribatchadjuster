﻿using System;
using System.Data;
using Utilities;

namespace Library
{

    public class Batch
    {
        private static Logger logger;
        private static PageInfo pi;

        private People people = null;
        private LedgerEntries ledgerEntries = null;

        public string BatchID { get; set; }

        public Batch(Logger _logger, PageInfo _pi)
        {
            logger = _logger;
            pi = _pi;
            BatchID = pi.BatchID;

            people = new People(logger, pi);
            ledgerEntries = new LedgerEntries(logger, pi);
            people.BatchID = BatchID;
        }

        public LedgerEntries GetInternalEntries()
        {
            return ledgerEntries;
        }

        public void GetPeopleFromBatch()
        {
            try
            {
                logger.LogMessage("Here in Batch.GetPeopleFromBatch()");
                string sql =
                    "select distinct l.nameid, n.firstname, n.lastname " +
                    "from rmledg l " +
                    "    inner join name n on l.nameid = n.nameid " +
                    "where l.rmbatchid = '{0}' ";
                sql = string.Format(sql, pi.BatchID);

                SqlAgent sa = new SqlAgent(logger);
                DataSet ds = sa.ExecuteQuery(sql, pi.ConnString);

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    Person p = new Person(logger, pi);
                    p.BatchId = BatchID;
                    p.NameID = dr[0].ToString();
                    p.FirstName = dr[1].ToString().Trim();
                    p.LastName = dr[2].ToString().Trim();
                    people.Add(p);
                }

                // For each of the people returned from the above query,
                // determine if any (or all) of them have an OpenAmt.  (Upnapid balance)
                people.FindOpenPayments();
                people.GetReceipts();
            }
            catch (Exception ex)
            {
                logger.LogMessage(string.Format("An error occured in MRIBatchAdjustER.Program.GetUnpostedBatches: {0}", ex.Message));
                throw;
            }
        }

        public void GetLedgerEntries()
        {                                       // Call GetLedgerEntries and...
            // stuff the list into local LEs.
            ledgerEntries.SetItems(LedgerEntries.GetLedgerEntries(logger, pi));
        }
    }
}
