﻿using System;
using System.Collections.Generic;
using System.Data;
using Utilities;

namespace Library
{
    public class LedgerEntries
    {
        private List<LedgerEntry> ledgerEntries = null;

        Logger logger;
        PageInfo pi;

        public LedgerEntries(Logger _logger, PageInfo _pi)
        {
            ledgerEntries = new List<LedgerEntry>();
            logger = _logger;
            pi = _pi;
        }

        public void Add(LedgerEntry ent)
        {
            ledgerEntries.Add(ent);
        }

        public List<LedgerEntry> GetItems()
        {
            return ledgerEntries;
        }

        public void SetItems(List<LedgerEntry> le)
        {
            ledgerEntries = le;
        }

        public decimal GetOpenAmount()
        {
            // loop thru ledgerEntries and return the sum OpenAmt
            return 0;
        }

        public static List<LedgerEntry> GetLedgerEntries(Logger logger, PageInfo pi)
        {
            List<LedgerEntry> retVal = new List<LedgerEntry>();

            try
            {
                string sql = "select tranid, rmpropid, nameid, trandate, chgcode, srccode, " +
                    "descrptn, tranamt, openamt, chkdesc, rmbatchid, refnmbr, period, posted " +
                    "from rmledg " +
                    "where rmbatchid = '{0}' ";
                sql = string.Format(sql, pi.BatchID);

                SqlAgent sa = new SqlAgent(logger);
                DataSet ds = sa.ExecuteQuery(sql, pi.ConnString);

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    LedgerEntry le = new LedgerEntry();
                    le.TranID = dr["tranid"].ToString();
                    le.PropertyID = dr["rmpropid"].ToString();
                    le.NameID = dr["nameid"].ToString();
                    le.TranDate = DateTime.Parse(dr["trandate"].ToString());
                    le.ChargeCode = dr["chgcode"].ToString();
                    le.SourceCode = dr["srccode"].ToString();
                    le.Description = dr["descrptn"].ToString();
                    le.TranAmount = decimal.Parse(dr["tranamt"].ToString());
                    le.OpenAmount = decimal.Parse(dr["openamt"].ToString());
                    le.CheckNumber = dr["chkdesc"].ToString();
                    le.BatchID = dr["rmbatchid"].ToString();
                    le.ReferenceNumber = dr["refnmbr"].ToString();
                    le.Period = dr["period"].ToString();
                    le.Posted = dr["posted"].ToString();
                    retVal.Add(le);
                }

            }
            catch (Exception ex)
            {
                logger.LogMessage(string.Format("An error occured in MRIBatchAdjustER.Program.GetUnpostedBatches: {0}", ex.Message));
                throw;
            }

            return retVal;
        }
    }
}
