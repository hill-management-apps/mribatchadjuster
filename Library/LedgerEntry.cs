﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Library
{
    public class LedgerEntry
    {
        public LedgerEntry()
        {
        }

        public LedgerEntry(DataRow dr)
        {
        }

        public string TranID { get; set; }
        public string PropertyID { get; set; }
        public string NameID { get; set; }
        public DateTime TranDate { get; set; }
        public string ChargeCode { get; set; }
        public string SourceCode { get; set; }
        public string Description { get; set; }
        public decimal TranAmount { get; set; }
        public decimal OpenAmount { get; set; }
        public string CheckNumber { get; set; }
        public string BatchID { get; set; }
        public string ReferenceNumber { get; set; }
        public string Period { get; set; }
        public string Posted { get; set; }

    }
}
