﻿using System.Collections.Generic;
using Utilities;

namespace Library
{

    
    public class People
    {
        private List<Person> people = new List<Person>();

        Logger logger = null;
        PageInfo pi = null;
        public string BatchID { get; set; }

        public People(Logger _logger, PageInfo _pi)
        {
            logger = _logger;
            pi = _pi;
        }

        public List<Person> GetList
        {
            get { return people; }
        }

        public void Add(Person p)
        {
            people.Add(p);
        }

        public void FindOpenPayments()
        {
            logger.LogMessage("Here in People.FindOpenPayments()");
            foreach (Person p in people)
            {
                p.DetermineIfNameHasBalance();
                logger.LogMessage(string.Format("Person {0} has an unpaid balance?  {1}", p.NameID.ToString(), p.HasOpenAmount));
            }
        }

        public void GetReceipts()
        {
            logger.LogMessage("Here in People.GetReceipts()");
            foreach (Person p in people)
            {
                p.GetReceipts();
            }
        }
    }
}
