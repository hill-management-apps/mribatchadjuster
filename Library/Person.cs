﻿using System;
using System.Data;
using Utilities;

namespace Library
{
    public class Person
    {
        Logger logger = null;
        PageInfo pi = null;
        public Receipts receipts = null;

        public Person(Logger _logger, PageInfo _pi)
        {
            logger = _logger;
            pi = _pi;
            receipts = new Receipts(logger, pi);
        }


        // Fetched from initial call to GetPeopleFromBatch()
        public string NameID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }


        // Fetched from second call to DetermineIfNameHasBalance()
        public DateTime TranDate { get; set; }
        public decimal OpenAmt { get; set; }
        public string BatchId { get; set; }

        public string FullName
        {
            get { return LastName + ", " + FirstName; }
        }

        public bool HasOpenAmount
        { 
            get { return this.OpenAmt != 0; } 
        }

        public void DetermineIfNameHasBalance()
        {
            try
            {
                logger.LogMessage("Here in Person.DetermineIfNameHasBalance()");
                string sql =
                    "select tranDate, nameid, sum(openamt) as SumOpen " +
                    "from rmledg " +
                    "where openamt <> 0 " +
                    "and nameid = '{0}' " +
                    "group by  tranDate, nameid " +
                    "order by tranDate, sumOpen desc,  nameid ";
                sql = string.Format(sql, this.NameID);

                SqlAgent sa = new SqlAgent(logger);
                DataSet ds = sa.ExecuteQuery(sql, pi.ConnString);

                // This query returns exactly one row per person.
                if (ds.Tables[0].Rows.Count != 0)
                {
                    this.TranDate = DateTime.Parse(ds.Tables[0].Rows[0]["tranDate"].ToString());
                    this.OpenAmt = Decimal.Parse(ds.Tables[0].Rows[0]["SumOpen"].ToString());
                }
                else
                {
                    this.OpenAmt = 0;
                }
            }
            catch (Exception ex)
            {
                logger.LogMessage(string.Format("An error occured in MRIBatchAdjustER.Program.GetUnpostedBatches: {0}", ex.Message));
                throw ex;
            }
        }
        public void GetReceipts()
        {
            receipts.GetReceipts(BatchId, NameID);
        }
    }
}
