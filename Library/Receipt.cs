﻿using System;
using System.Collections.Generic;
using System.Data;
using Utilities;

namespace Library
{
    public class Receipt
    {
        Logger logger = null;
        PageInfo pi = null;
        public List<ReceiptDetail> Items = null;

        public Receipt(Logger _logger, PageInfo _pi)
        {
            logger = _logger;
            pi = _pi;
            Items = new List<ReceiptDetail>();
        }

        public string ReceiptNumber { get; set; }
        public DateTime TranDate { get; set; }
        public decimal TranAmt { get; set; }
        public string CheckNumber { get; set; }

        public void GetReceiptItems(string batchId, string nameId, string recptno)
        {
            try
            {
                logger.LogMessage("Here in Receipt.GetReceiptItems()");
                string sql =
                    "select * from rmrcpt " +
                    "where rmbatchid = '{0}' " +
                    "and nameid = '{1}' " +
                    "and recptno = '{2}' " +
                    "order by rmbatchid, recptno, item ";
                sql = string.Format(sql, batchId, nameId, recptno);

                SqlAgent sa = new SqlAgent(logger);
                DataSet ds = sa.ExecuteQuery(sql, pi.ConnString);

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    ReceiptDetail rd = new ReceiptDetail(logger, pi);
                    rd.item = int.Parse(dr["item"].ToString());
                    rd.ChargeCode = dr["chgcode"].ToString();
                    rd.TranAmt = Decimal.Parse(dr["tranamt"].ToString());
                    rd.RefNumber = dr["refnmbr"].ToString();
                    rd.TranID = dr["tranid"].ToString();
                    Items.Add(rd);
                }
            }
            catch (Exception ex)
            {
                logger.LogMessage(string.Format("An error occured in MRIBatchAdjustER.Program.GetUnpostedBatches: {0}", ex.Message));
                throw ex;
            }
        }
    }
}
