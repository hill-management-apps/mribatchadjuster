﻿using Utilities;

namespace Library
{
    public class ReceiptDetail
    {
        Logger logger = null;
        PageInfo pi = null;

        public ReceiptDetail(Logger _logger, PageInfo _pi)
        {
            logger = _logger;
            pi = _pi;
        }

        public int item { get; set; }
        public string ChargeCode { get; set; }
        public string SourceCode { get; set; }
        public decimal TranAmt { get; set; }
        public string RefNumber { get; set; }
        public string TranID { get; set; }

    }
}
