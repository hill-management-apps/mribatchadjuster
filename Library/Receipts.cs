﻿using System;
using System.Collections.Generic;
using System.Data;
using Utilities;

namespace Library
{
    public class Receipts
    {
        Logger logger = null;
        PageInfo pi = null;
        public List<Receipt> receipts = null;

        public Receipts(Logger _logger, PageInfo _pi)
        {
            logger = _logger;
            pi = _pi;
            receipts = new List<Receipt>();

        }


        public void GetReceipts(string batchId, string nameId)
        {
            try
            {
                logger.LogMessage("Here in Receipts.GetReceipts()");
                string sql =
                    "select * from rmrecpt " +
                    "where rmbatchid = '{0}' " +
                    "and nameid = '{1}' ";
                sql = string.Format(sql, batchId, nameId);

                SqlAgent sa = new SqlAgent(logger);
                DataSet ds = sa.ExecuteQuery(sql, pi.ConnString);

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    Receipt r = new Receipt(logger, pi);
                    r.ReceiptNumber = dr["recptno"].ToString();
                    r.TranDate = DateTime.Parse(dr["trandate"].ToString());
                    r.TranAmt = Decimal.Parse(dr["tranamt"].ToString());
                    r.CheckNumber = dr["checkno"].ToString();
                    receipts.Add(r);
                }

                // Fetch the detail items
                foreach (Receipt r in receipts)
                    r.GetReceiptItems(batchId, nameId, r.ReceiptNumber);
            }
            catch (Exception ex)
            {
                logger.LogMessage(string.Format("An error occured in MRIBatchAdjustER.Program.GetUnpostedBatches: {0}", ex.Message));
                throw ex;
            }
        }
    }
}
