﻿using System;
using System.Data;
using Utilities;
using Library;

namespace MRIBatchAdjustER
{
    class Program
    {
        const string CONNECTION_STRING = @"data source=HSSQL2012\HSSQLSERVER;initial catalog={0};Integrated Security=true;";
        private static PageInfo pi = new PageInfo();
        public static Logger logger = null;
        public static SqlAgent sa = null;
        public static DateTime dtStart = DateTime.Now;

        // This program will take exactly two arguments:
        //  - [0] = the Initial Database Catalog
        //  - [1] = The Period to process
        static void Main(string[] args)
        {
            try
            {
                pi.ExcelFileName = @"C:\HillMgt Automation\HillEnterpriseReporting\MRIBatchAdjustER\MRIBatchAdjust.xlsx";
                pi.EchoToConsole = true;
                pi.LogFileName = @"C:\HillMgt Automation\HillEnterpriseReporting\MRIBatchAdjustER\MRIBatchAdjust.log";

                // Make the logger
                logger = new Logger(pi);
                logger.LogMessage("MRIBatchAdjustER has started.");

                // Ensure there are two args.
                if (args.Length != 2)
                {
                    logger.LogMessage(string.Format("This program requires three arguments but only received {0}", args.Length.ToString()));
                    Environment.Exit(-1);
                }

                // Make the connection string and a
                pi.ConnString = string.Format(CONNECTION_STRING, args[0]);
                pi.BatchID = args[1];

                //==================================================================================
                // == The first step here is to read in the information from the Batch, the       ==
                // == people who's checks are included in the batch, and for each person, a       ==
                // == Receipt with detail of the ledger items that were included in the payment   ==
                //==================================================================================
                Batch batch = new Batch(logger, pi);
                batch.GetPeopleFromBatch();
                batch.GetLedgerEntries();
                
                // For inspecting ledger entries during debugging
                //LedgerEntries les = batch.GetInternalEntries();

                //==================================================================================
                // == The second step is to reconstruct the ledger as it appeared BEFORE the user ==
                // == SAVED the Batch.  Not with real rows, but in memory using the LedgerEntries ==
                // == and LedgerEntry objects.  Having this 'viirtual ledger' we can then examine ==
                // == the ledger as it appeared before MRI applied it's logic as to which         ==
                // == of the tenant's expenses to pay and in what order.                          ==
                //==================================================================================

                // For each open, unposted batch we find in the DB, call
                // LoadDataRows on the ExcelAgent.
            }
            catch (Exception ex)
            {
                logger.LogMessage(string.Format("An error occurred in MRIBatchAdjustER: {0}", ex.Message));
            }
            finally
            {
                TimeSpan ts = DateTime.Now - dtStart;
                logger.LogMessage(string.Format("MRIBatchAdjustR Terminated.  Runtime:  {0} seconds.", ts.TotalSeconds));
            }
        }


        public static DataSet GetUnpostedBatches(PageInfo pi)
        {
            try
            {
                string sql =
                    "select rmBatchId as BatchID, l.nameid, cc.DESCRPTN, cc.priority " +
                    "from rmledg l " +
                    "    inner join chgcode cc on l.chgcode = cc.chgcode " +
                    "where period = {0} " +
                    "and openamt > 0 " +
                    "and posted<> 'Y' " +
                    "order by BatchID, l.nameid, cc.priority desc ";
                sql = string.Format(sql, pi.Period);

                sa = new SqlAgent(logger);
                return (sa.ExecuteQuery(sql, pi.ConnString));

            }
            catch (Exception ex)
            {
                logger.LogMessage(string.Format("An error occured in MRIBatchAdjustER.Program.GetUnpostedBatches: {0}", ex.Message));
                throw ex;
            }
        }
    }
}
