﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;
using System.Net.Mail;
using System.Data.SqlClient;

namespace Utilities
{
    public class EmailAgent
    {
        private const string OFFICE365_SMTP_SERVER = "Smtp.office365.com";

        private const string FROM_EMAIL_ADDR = "donotreply@peakmgt.com";
        private const string EMAIL_SUBJECT = "Charge Reconciiation Report";
        private const string EMAIL_BODY = "Attached, please find an Excel Spreadsheet.";

        private Logger logger;


        public EmailAgent(Logger sfLogger)
        {
            logger = sfLogger;
        }

        public void SendEmail(string attachmentPath, string toAddress)
        {
            try
            {
                logger.LogMessage(string.Format("About to send email {0}  to  bankrecs@hillmgt.com", EMAIL_SUBJECT));
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(OFFICE365_SMTP_SERVER);

                mail.From = new MailAddress(FROM_EMAIL_ADDR);
                mail.To.Add(toAddress);

                mail.Subject = EMAIL_SUBJECT;
                mail.Body = EMAIL_BODY;

                Attachment a = new Attachment(attachmentPath);
                mail.Attachments.Add(a);

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("donotreply@peakmgt.com", "Pwdnr593#");      // TODO
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);
            }
            catch (Exception _ex)
            {
                logger.LogMessage(string.Format("=====> Error occured in EmailAgent.SendEmail: {0}", _ex.Message));
                throw _ex;
            }
        }

        public void ExecutNonQuery(string sql, string connString)
        {
            logger.LogMessage("Here in SqlAgent.ExecuteNonQuery");

            try
            {
                SqlConnection cn = new SqlConnection(connString);
                SqlCommand command = new SqlCommand(sql, cn);
                command.Connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception _ex)
            {
                logger.LogMessage(string.Format("=====> Error occured in SqlAgent.ExecuteQuery: {0}", _ex.Message));
                throw _ex;
            }
        }

    }
}
