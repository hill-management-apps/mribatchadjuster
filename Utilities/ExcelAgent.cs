﻿using System;
using Microsoft.Office.Interop.Excel;

namespace Utilities
{

    public class ExcelAgent
    {
        private Application app;
        private Workbook wb;
        private Worksheet ws;

        private Logger logger = null;
        private PageInfo pi = null;

        public ExcelAgent(Logger sfLogger, PageInfo _pi)
        {
            logger = sfLogger;
            pi = _pi;
        }

        #region Create App, Workbook and Worksheet Objects
        public void ApplyInitialFormat()
        {
            try
            {
                logger.LogMessage("ExcelAgent.ApplyInitialFormat");

                // Previous sheet name being empty indicates the 
                // first time thru
                // Create the  necessary junk for Excel to run
                app = new ApplicationClass();       // App
                                                    //app.DisplayAlerts = false;
                                                    //app.ScreenUpdating = false;

                //===================
                // Visible
                //===================
                app.Visible = true;
                app.UserControl = true;
                app.Interactive = true;

                //===================
                // Invisible
                //===================
                //app.DisplayAlerts = false;
                //app.ScreenUpdating = false;
                //app.Visible = false;
                //app.UserControl = false;
                //app.Interactive = false;


                wb = app.Workbooks.Add();           // Workbook
                ws = (Worksheet)wb.Sheets["Sheet1"];
            }
            catch (Exception _ex)
            {
                logger.LogMessage(string.Format("=====> Error occured in ExcelAgent.ApplyInitialFormat: {0}", _ex.Message));
                throw _ex;
            }
        }
        #endregion

        #region Load Data Rows
        public void LoadDataRows(string batchID)
        {
            Range r = null;
            bool requiresAdjustment = true;

            try
            {
                logger.LogMessage("ExcelAgent.LoadDataRows");

                // Fetch the receipt rows for this batchid

                // Determine if the receipts are in the correct order.
                // If so, return.
                if (!requiresAdjustment)
                    return;

                // Otherwise, reorder the receipt rows
                AdjustReceiptRows();

            }
            catch (Exception _ex)
            {
                logger.LogMessage(string.Format("=====> Error occured in ExcelAgent.LoadDataRows: {0}", _ex.Message));
                throw _ex;
            }
        }
        #endregion

        #region Save and Close
        public void SaveAndClose(PageInfo pi)
        {
            try
            {
                logger.LogMessage(string.Format("ExcelAgent.SaveAndClose - File: {0}", pi.ExcelFileName));

                wb.SaveAs(pi.ExcelFileName, XlFileFormat.xlWorkbookDefault);
                wb.Close();
                app.Quit();
           }
            catch (Exception _ex)
            {
                logger.LogMessage(string.Format("=====> Error occured in ExcelAgent.SaveAndClose: {0}", _ex.Message));
                throw _ex;
            }
        }
        #endregion

        #region Adjust Reciept Rows
        private void AdjustReceiptRows()
        {
            try
            {
            }
            catch (Exception _ex)
            {
                logger.LogMessage(string.Format("=====> Error occured in ExcelAgent.AdjustReceiptRows: {0}", _ex.Message));
                throw _ex;
            }
        }
        #endregion
    }
}
