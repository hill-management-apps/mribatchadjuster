﻿using System;
using System.IO;

namespace Utilities
{
    public class Logger
    {
        private PageInfo pi = null;

        public Logger(PageInfo _pi)
        {
            pi = _pi;
        }

        public void LogMessage(string msg)
        {
            try
            {
                File.AppendAllText(pi.LogFileName, string.Format("{0}\t{1}\n", DateTime.Now.ToLongTimeString(), msg));

                if (pi.EchoToConsole)
                    Console.WriteLine(string.Format("{0}\t{1}", DateTime.Now.ToLongTimeString(), msg));
            }
            catch
            {
                throw;
            }
        }
    }
}
