﻿using System;

namespace Utilities
{
    public class PageInfo
    {
        public string LogFileName = string.Empty;
        public bool EchoToConsole = false;
        public string ExcelFileName = string.Empty;
        public string Period = string.Empty;
        public string ConnString = string.Empty;
        public string BatchID = string.Empty;
        public string NameID = string.Empty;

        public PageInfo()
        {
        }

        public DateTime PeriodAsDate
        {
            get
            {
                return (DateTime.Parse(string.Format("{0}/{1}/{2}",
                    int.Parse(this.Period.Substring(4, 2)), 1, int.Parse(this.Period.Substring(0, 4)))));
            }
        }
    }
}
