﻿using System;
using System.Data;
using System.Data.SqlClient;



namespace Utilities
{
    public class SqlAgent
    {
        private Logger logger = null;

        public SqlAgent(Logger _logger)
        {
            logger = _logger;
        }

        public DataSet ExecuteQuery(string sql, string connectString)
        {
            DataSet ds = new DataSet();
            //logger.LogMessage("Here in SqlAgent.ExecuteQuery");

            try
            {
                SqlDataAdapter da = new SqlDataAdapter(sql, connectString);
                da.Fill(ds);
                return ds;
            }
            catch (Exception _ex)
            {
                logger.LogMessage(string.Format("=====> Error occured in SqlAgent.ExecuteQuery: {0}", _ex.Message));
                throw _ex;
            }
        }

        public void ExecuteNonQuery(string sql, string connString)
        {
            logger.LogMessage("Here in SqlAgent.ExecuteNonQuery");

            try
            {
                SqlConnection cn = new SqlConnection(connString);
                SqlCommand command = new SqlCommand(sql, cn);
                command.Connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception _ex)
            {
                logger.LogMessage(string.Format("=====> Error occured in SqlAgent.ExecuteNonQuery: {0}", _ex.Message));
                throw _ex;
            }
        }
    }
}
